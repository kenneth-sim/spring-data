package com.kenneth.springdata.mongo.controllers;

import com.kenneth.springdata.mongo.models.Group;
import com.kenneth.springdata.mongo.models.User;
import com.kenneth.springdata.mongo.services.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

@RestController
public class TestController {

    private static final List<String> FIRST_NAMES = Arrays.asList(
            "John", "Jane", "Alex", "Chris", "Taylor", "Jordan", "Morgan", "Pat", "Casey", "Riley", "Benjamin", "Shaun"
    );

    private static final List<String> LAST_NAMES = Arrays.asList(
            "Smith", "Johnson", "Williams", "Brown", "Jones", "Garcia", "Miller", "Davis", "Martinez", "Wilson", "Mendes"
    );


    private static final Random RANDOM = new Random();

    private static StringBuilder STRING_BUILDER;

    @Autowired
    private TestService testService;

    @PostMapping("/save")
    public void saveUser(){
        STRING_BUILDER = new StringBuilder(FIRST_NAMES.get(RANDOM.nextInt(FIRST_NAMES.size())));
        String name = String.valueOf(STRING_BUILDER.append(" ").append(LAST_NAMES.get(RANDOM.nextInt(FIRST_NAMES.size()))));

        int max = 15000;
        int min = 3000;

        LocalDate startDate = LocalDate.of(1970, 1, 1);  // Start date (inclusive)
        LocalDate endDate = LocalDate.of(2005, 12, 31);
        long daysBetween = ChronoUnit.DAYS.between(startDate, endDate);
        long randomDays = ThreadLocalRandom.current().nextLong(daysBetween + 1); // +1 to include the endDate
        LocalDate randomBirthdate = startDate.plusDays(randomDays);

        UUID uuid = UUID.randomUUID();
        User user = User.builder()
                .id(uuid.toString())
                .name(name)
                .salary(RANDOM.nextInt((max - min) + 1) + min)
                .birthDate(randomBirthdate)
                .group(Group.values()[RANDOM.nextInt(Group.values().length)])
                .ts(LocalDateTime.now())
                .build();
        testService.saveUser(user);
    }

    @GetMapping("/{pageNo}")
    public Page<User> getUsers(@PathVariable int pageNo){
        Sort sort = Sort.by(Sort.Direction.DESC, "ts");
        Pageable pageable = PageRequest.of(pageNo - 1, 3, sort);

        Page<User> userPage = testService.getUsers(pageable);
        return userPage;
    }

    @DeleteMapping
    public void deleteUsers(){
        testService.deleteAll();
    }

    @GetMapping("/age-range")
    public List<User> getUsersByAgeRange(
            @RequestParam int ageFrom,
            @RequestParam int ageTo
    ){
        return testService.getUsers(ageFrom, ageTo);
    }

    @GetMapping("/report")
    public List<TestService.GroupRecord> getGroupReport(){
        return testService.getGroupReport();
    }

}
