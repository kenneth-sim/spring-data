package com.kenneth.springdata.mongo.models;

public enum Group {
    BADMINTON,
    SQUASH,
    SWIMMING
}
