package com.kenneth.springdata.mongo.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document
public class User {
    private String id;
    private String name;
    private LocalDate birthDate;
    private int salary;
    private Group group;
    private LocalDateTime ts;
}

