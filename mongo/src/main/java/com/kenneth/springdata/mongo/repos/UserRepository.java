package com.kenneth.springdata.mongo.repos;

import com.kenneth.springdata.mongo.models.User;
import com.kenneth.springdata.mongo.services.TestService;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface UserRepository extends MongoRepository<User, String> {
    @Aggregation(pipeline = {
            "{$addFields: { age: { $subtract: [ {$year: new Date()}, {$year: '$birthDate'}]}}}",
            "{$match: { age: {$gte: ?0, $lte: ?1}}}",
            "{$sort: {name: 1}}",
            "{$project: {age: 0}}"
    })
    List<User> findUserInAgeRange(int fromAge, int toAge);

    @Aggregation(pipeline = {
            "{$addFields: {age: { $subtract: [{$year: new Date()}, {$year: '$birthDate'}]}}}",
            "{$group: {_id: '$group', avgEarning: {$avg: '$salary'}, avgAge: {$avg: '$age'}}}",
            "{$project: {group: '$_id', avgEarning: 1, avgAge: 1, _id: 0}}"
    })
    List<TestService.GroupRecord> getGroupReport();
}
