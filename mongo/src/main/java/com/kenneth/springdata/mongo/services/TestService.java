package com.kenneth.springdata.mongo.services;

import com.kenneth.springdata.mongo.models.Group;
import com.kenneth.springdata.mongo.models.User;
import com.kenneth.springdata.mongo.repos.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TestService {
    public record GroupRecord(Group group, Integer avgEarning, Integer avgAge){};

    @Autowired
    private UserRepository userRepository;

    public void saveUser(User user){
        userRepository.save(user);
    }

    public Page<User> getUsers(Pageable pageable){
        Page<User> userPage = userRepository.findAll(pageable);
        return userPage;
    }

    public void deleteAll(){
        userRepository.deleteAll();
    }

    public List<User> getUsers(int fromAge, int toAge){
        return userRepository.findUserInAgeRange(fromAge, toAge);
    }

    public List<GroupRecord> getGroupReport(){
        return userRepository.getGroupReport();
    }
}
